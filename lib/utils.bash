#!/usr/bin/env bash

set -euo pipefail

GH_REPO="https://github.com/ogham/exa"

fail() {
  echo -e "asdf-exa: $*"
  exit 1
}

curl_opts=(-fsSL)

# NOTE: You might want to remove this if exa is not hosted on GitHub releases.
if [ -n "${GITHUB_API_TOKEN:-}" ]; then
  curl_opts=("${curl_opts[@]}" -H "Authorization: token $GITHUB_API_TOKEN")
fi

sort_versions() {
  sed 'h; s/[+-]/./g; s/.p\([[:digit:]]\)/.z\1/; s/$/.z/; G; s/\n/ /' |
    LC_ALL=C sort -t. -k 1,1 -k 2,2n -k 3,3n -k 4,4n -k 5,5n | awk '{print $2}'
}

list_github_tags() {
  git ls-remote --tags --refs "$GH_REPO" |
    grep -o 'refs/tags/.*' | cut -d/ -f3- |
    sed 's/^v//' # NOTE: You might want to adapt this sed to remove non-version strings from tags
}

list_all_versions() {
  # TODO: Adapt this. By default we simply list the tag names from GitHub releases.
  # Change this function if exa has other means of determining installable versions.
  list_github_tags
}

download_release() {
  local version filename url
  version="$1"
  filename="$2"

  # TODO: Adapt the release URL convention for exa
  # https://github.com/ogham/exa/releases/download/v0.9.0/exa-linux-x86_64-0.9.0.zip
  url="$GH_REPO/releases/download/v${version}/exa-linux-x86_64-${version}.zip"

  echo "* Downloading exa release $version..."
  curl "${curl_opts[@]}" -o "$filename" -C - "$url" || fail "Could not download $url"
}

install_version() {
  local install_type="$1"
  local version="$2"
  local install_path="$3"

  if [ "$install_type" != "version" ]; then
    fail "asdf-exa supports release installs only"
  fi

  local release_file="$install_path/exa-linux-x86_64-$version.zip"
  (
    mkdir -p "$install_path"
    download_release "$version" "$release_file"
    unzip ${release_file} -d ${install_path}
    mkdir -p ${install_path}/bin
    mv ${install_path}/exa-linux-x86_64 ${install_path}/bin/exa
    rm "$release_file"

    local tool_cmd
    tool_cmd="exa"
    test -x "$install_path/bin/${tool_cmd}" || fail "Expected $install_path/bin/${tool_cmd} to be executable."

    echo "exa $version installation was successful!"
  ) || (
    rm -rf "$install_path"
    fail "An error ocurred while installing exa $version."
  )
}
